"use strict";

const deps = {
    TypicaLisp: require('TypicaLisp.js'),
    Scope: require('Scope.js'),
    _: require('lodash'),
    assert: require('assert'),
    util: require('util'),
    Value: require('Value.js'),
    TypeTable: require('TypeTable.js'),
};

function inspect(i) { return deps.util.inspect(i); }
function i(i) { return deps.Value.create(deps.TypeTable.create().findTypeByName('Int'), i); }
function f(f) { return deps.Value.create(deps.TypeTable.create().findTypeByName('Float'), f); }
function s(s) { return deps.Value.create(deps.TypeTable.create().findTypeByName('String'), s); }
function b(b) { return deps.Value.create(deps.TypeTable.create().findTypeByName('Bool'), b); }
function l(l) { return deps.Value.create(deps.TypeTable.create().findTypeByName('List'), l); }

const tl = deps.TypicaLisp.create();
const rv = tl.run(`(read-line (lambda (' (: String s)) Object (write-line s)))`, deps.Scope.createGlobalScope());
//const rv = tl.run(`(read-line (lambda (' (: String s)) Object (+ 2 3)))`, deps.Scope.createGlobalScope());
//const rv = tl.run(`(read-line write-line)`, deps.Scope.createGlobalScope());
console.log('rv', inspect(rv));

