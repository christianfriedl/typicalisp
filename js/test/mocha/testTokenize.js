"use strict";

const deps = {
    SimpleLisp: require('SimpleLisp.js'),
    _: require('lodash'),
    assert: require('assert'),
};
describe('tokenizer - atoms', function() {
    it('should tokenize a simple int atom', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('1');
        const expectedTokens = [ 
            { word: '1', tokenType: 'literal', dataType: 'Int' },
            { word: '', tokenType: 'end', dataType: undefined } 
        ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it('should tokenize a simple float atom', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('1.2');
        const expectedTokens = [ 
            { word: '1.2', tokenType: 'literal', dataType: 'Float' },
            { word: '', tokenType: 'end', dataType: undefined } 
        ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it('should tokenize a simple string atom', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('"text"');
        const expectedTokens = [ 
            { word: 'text', tokenType: 'literal', dataType: 'String' },
            { word: '', tokenType: 'end', dataType: undefined } 
        ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
});
describe('tokenizer - sexprs', function() {
    it('should tokenize a simple sexpr', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('(+ 1 2)');
        const expectedTokens = [ 
            { word: '(', tokenType: 'open_sexpr', dataType: null },
            { word: '+', tokenType: 'word', dataType: null },
            { word: 1, tokenType: 'literal', dataType: 'Int' },
            { word: 2, tokenType: 'literal', dataType: 'Int' },
            { word: ')', tokenType: 'close_sexpr', dataType: null },
            { word: '', tokenType: 'end', dataType: undefined } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it('should tokenize a nested sexpr', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('(+ (+ 1 2) 3)');
        const expectedTokens = [ { word: '(', tokenType: 'open_sexpr', dataType: null },
            { word: '+', tokenType: 'word', dataType: null },
            { word: '(', tokenType: 'open_sexpr', dataType: null },
            { word: '+', tokenType: 'word', dataType: null },
            { word: 1, tokenType: 'literal', dataType: 'Int' },
            { word: 2, tokenType: 'literal', dataType: 'Int' },
            { word: ')', tokenType: 'close_sexpr', dataType: null },
            { word: 3, tokenType: 'literal', dataType: 'Int' },
            { word: ')', tokenType: 'close_sexpr', dataType: null },
            { word: '', tokenType: 'end', dataType: undefined } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
});
describe.skip('tokenizer - lists', function() {
    it.skip('should tokenize a simple list', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('[1 2 3]');
        const expectedTokens = 
[ { word: '[', tokenType: 'open_list' },
  { word: '1', tokenType: 'word' },
  { word: '2', tokenType: 'word' },
  { word: '3', tokenType: 'word' },
  { word: ']', tokenType: 'close_list' },
  { word: '', tokenType: 'end' } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it.skip('should tokenize a simple list w/strings', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('["a" "b" "c"]');
        const expectedTokens = 
[ { word: '[', tokenType: 'open_list' },
  { word: 'a', tokenType: 'word' },
  { word: 'b', tokenType: 'word' },
  { word: 'c', tokenType: 'word' },
  { word: ']', tokenType: 'close_list' },
  { word: '', tokenType: 'end' } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it.skip('should tokenize a complex list', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('[1 2 [3 4]]');
        const expectedTokens = 
[ { word: '[', tokenType: 'open_list' },
  { word: '1', tokenType: 'word' },
  { word: '2', tokenType: 'word' },
  { word: '[', tokenType: 'open_list' },
  { word: '3', tokenType: 'word' },
  { word: '4', tokenType: 'word' },
  { word: ']', tokenType: 'close_list' },
  { word: ']', tokenType: 'close_list' },
  { word: '', tokenType: 'end' } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it.skip('should tokenize a list/sexpr text', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('( [1 2 [3 4]] )');
        const expectedTokens = 
[ { word: '(', tokenType: 'open_sexpr' },
  { word: '[', tokenType: 'open_list' },
  { word: '1', tokenType: 'word' },
  { word: '2', tokenType: 'word' },
  { word: '[', tokenType: 'open_list' },
  { word: '3', tokenType: 'word' },
  { word: '4', tokenType: 'word' },
  { word: ']', tokenType: 'close_list' },
  { word: ']', tokenType: 'close_list' },
  { word: ')', tokenType: 'close_sexpr' },
  { word: '', tokenType: 'end' } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
    it.skip('should tokenize a list/sexpr text 2', function() {
        const si = deps.SimpleLisp;
        const tokens = si.tokenize('( [1 2 [(3 5) 4]] )');
        const expectedTokens = 
[ { word: '(', tokenType: 'open_sexpr' },
  { word: '[', tokenType: 'open_list' },
  { word: '1', tokenType: 'word' },
  { word: '2', tokenType: 'word' },
  { word: '[', tokenType: 'open_list' },
  { word: '(', tokenType: 'open_sexpr' },
  { word: '3', tokenType: 'word' },
  { word: '5', tokenType: 'word' },
  { word: ')', tokenType: 'close_sexpr' },
  { word: '4', tokenType: 'word' },
  { word: ']', tokenType: 'close_list' },
  { word: ']', tokenType: 'close_list' },
  { word: ')', tokenType: 'close_sexpr' },
  { word: '', tokenType: 'end' } ];

        deps.assert.deepEqual(expectedTokens, tokens);
    });
});
