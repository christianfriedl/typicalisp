"use strict";

const deps = {
    Type: require('Type.js'),
    TypeExpression: require('TypeExpression.js'),
    MetaType: require('MetaType.js'),
    assert: require('assert'),
};

const assert = deps.assert;

describe('Type creation', function() {
    it('should create a simple Type', function() {
        const type = deps.Type.create('Foo');
        assert.strictEqual('Type', type.dataType);
        assert.strictEqual('Foo', type.getName());
        assert.deepEqual([], type.orTypes);
        assert.deepEqual([], type.notTypes);
    });
    it.skip('should disallow creation of type circles', function() {
        // TODO - how to actually create this scenario in the test?
        const subSubType = deps.Type.create('SubSubFoo');
        const subType = deps.Type.create('SubFoo', [ deps.TypeExpression.create(subSubType, 'or') ]);
        const fooType = deps.Type.create('Foo', [ deps.TypeExpression.create(subType, 'or') ]);
    });
});

describe('Composed types', function() {
    it('should add an or-expression to an existing type', function() {
        const fooType = deps.Type.create('Foo');
        assert.strictEqual('Type', fooType.dataType);
        assert.strictEqual('Foo', fooType.getName());

        const barType = deps.Type.create('Bar');
        assert.strictEqual('Type', barType.dataType);
        assert.strictEqual('Bar', barType.getName());
        fooType.addOrType(barType);
        assert.deepEqual([ barType ], fooType.getOrTypes(), 'Foo now has Bar in its or-types');
    });
});
describe('Parameterized types', function() {
    it('should add a concrete type parameter to an existing type', function() {
        const fooType = deps.Type.create('Foo');
        fooType.setParameterNames(['a']);
        const barType = deps.Type.create('Bar');
        fooType.setVariant({ 'a': barType });
    });
    it('should add a concrete type parameter list to an existing type', function() {
        const fooType = deps.Type.create('Foo');
        fooType.setParameterNames(['a']);
        const barType = deps.Type.create('Bar');
        fooType.setVariant({'a': [barType]});
    });
    it('should add a variable type parameter to an existing type', function() {
        const fooType = deps.Type.create('Foo');
        fooType.setParameterNames(['a']);
        fooType.setVariant({'a': 'b'});
    });
});

describe('Type matching', function() {
    it('should match itself', function() {
        const fooType = deps.Type.create('Foo');
        const fooType2 = fooType;
        assert.ok(fooType.match(fooType2));
    });
    it('should match a type in its or-types', function() {
        const subType = deps.Type.create('SubFoo');
        const fooType = deps.Type.create('Foo');
        fooType.addOrType(subType);
        assert.ok(fooType.match(subType));
    });
    it('should match a type among several in its or-types', function() {
        const subType1 = deps.Type.create('SubFoo1');
        const subType2 = deps.Type.create('SubFoo2');
        const fooType = deps.Type.create('Foo');
        fooType.addOrType(subType1);
        fooType.addOrType(subType2);
        assert.ok(fooType.match(subType2));
    });
    it('should recursively match a type in its or-types', function() {
        const subSubType = deps.Type.create('SubSubFoo');
        const subType = deps.Type.create('SubFoo');
        subType.addOrType(subSubType);
        const fooType = deps.Type.create('Foo');
        fooType.addOrType(subType);
        assert.ok(fooType.match(subSubType));
    });
});
describe('Parameterized Type Matching', function() {
    const TObject = deps.Type.create('Object');

    const TNumber = deps.Type.create('Number');
    TObject.addOrType(TNumber);


    const TInt = deps.Type.create('Int');
    TNumber.addOrType(TInt);

    const TFloat = deps.Type.create('Float');
    TNumber.addOrType(TFloat);

    const TString = deps.Type.create('String');
    TObject.addOrType(TString);


    const TListNumber = deps.Type.create('List');
    TListNumber.setParameterNames(['el']);
    TListNumber.setVariant({'el': TNumber});

    const TListInt = deps.Type.create('List');
    TListInt.setParameterNames(['el']);
    TListInt.setVariant({'el': TInt});

    const TListString = deps.Type.create('List');
    TListString.setParameterNames(['el']);
    TListString.setVariant({'el': TString});

    const TListFloat = deps.Type.create('List');
    TListFloat.setParameterNames(['el']);
    TListFloat.setVariant({'el': TFloat});

    const TListA = deps.Type.create('List');
    TListA.setParameterNames(['el']);
    TListA.setVariant({ 'el': 'a'});

    const TListB = deps.Type.create('List');
    TListB.setParameterNames(['el']);
    TListB.setVariant({ 'el': 'b'});


    const THashStringInt = deps.Type.create('Hash');
    THashStringInt.setParameterNames(['key', 'value']);
    THashStringInt.setVariant({ 'key': TString, 'value': TInt });

    const THashStringA = deps.Type.create('Hash');
    THashStringA.setParameterNames(['key', 'value']);
    THashStringA.setVariant({key: TString, value: 'a'});

    const THashStringB = deps.Type.create('Hash');
    THashStringB.setParameterNames(['key', 'value']);
    THashStringB.setVariant({key: TString, value: 'b'});

    const THashStringString = deps.Type.create('Hash');
    THashStringString.setParameterNames(['key', 'value']);
    THashStringString.setVariant({key: TString, value: TString});

    const THashAA = deps.Type.create('Hash');
    THashAA.setParameterNames(['key', 'value']);
    THashAA.setVariant({key: 'a', value: 'a'});

    const THashAB = deps.Type.create('Hash');
    THashAB.setParameterNames(['key', 'value']);
    THashAB.setVariant({key: 'a', value: 'b'});


    const TClosureInt = deps.Type.create('Closure');
    TClosureInt.setParameterNames(['parameters']);
    TClosureInt.setVariant({parameters: [TInt]});

    const TClosureString = deps.Type.create('Closure');
    TClosureString.setParameterNames(['parameters']);
    TClosureString.setVariant({parameters: [TString]});

    const TClosureAA = deps.Type.create('Closure');
    TClosureAA.setParameterNames(['parameters', 'returnType']);
    TClosureAA.setVariant({parameters: ['a'], returnType: 'a'});

    const TClosureAB = deps.Type.create('Closure');
    TClosureAB.setParameterNames(['parameters', 'returnType']);
    TClosureAB.setVariant({parameters: ['a'], returnType: 'b'});

    it('should match List(Int) <- List(Int)', function() {
        assert.ok(TListInt.match(TListInt));
    });
    // List(Int) <- List(Float) FAIL -- unrelated parameters
    it('should not match List(Int) <- List(Float)', function() {
        assert.ok(! TListInt.match(TListFloat));
    });
    // Hash(Foo, Bar) <- List(Foo) FAIL -- unrelated types
    it('should not match Hash(Foo, Bar) <- List(Foo)', function() {
        assert.ok(! THashStringInt.match(TListFloat));
    });
    // List(a) <- List(Int) OK
    it('should match List(a) <- List(Int)', function() {
        assert.ok(TListA.match(TListInt));
    });
    // List(a) <- List(b) OK
    it('should match List(a) <- List(b)', function() {
        assert.ok(TListA.match(TListB));
    });
    // List(Int) <- List(a) FAIL
    it('should not match List(Int) <- List(a)', function() {
        assert.ok(! TListInt.match(TListA));
    });
    // List(Number) <- List(Int) OK -- types are ORed
    it('should match List(Number) <- List(Int)', function() {
        assert.ok(TListNumber.match(TListInt));
    });
    // List(Int) <- List(String) FAIL -- unrelated types
    it('should not match List(Int) <- List(String)', function() {
        assert.ok(! TListInt.match(TListString));
    });
    // Hash(String, Int) <- Hash(String, a) FAIL
    it('should not match Hash(String, Int) <- Hash(String, a)', function() {
        assert.ok(! THashStringInt.match(THashStringA));
    });
    // Hash(String, a) <- Hash(String, Int) OK
    it('should match Hash(String, a) <- Hash(String, Int)', function() {
        assert.ok(THashStringA.match(THashStringInt));
    });
    // Hash(String, a) <- Hash(String, b) OK
    it('should match Hash(String, a) <- Hash(String, b)', function() {
        assert.ok(THashStringA.match(THashStringB));
    });
    // Hash(a, a) <- Hash(String, String) OK
    it('should match Hash(a, a) <- Hash(String, String)', function() {
        assert.ok(THashAA.match(THashStringString));
    });
    // Hash(a, b) <- Hash(String, String) OK
    it('should match Hash(a, b) <- Hash(String, String)', function() {
        assert.ok(THashAB.match(THashStringString));
    });
    // Hash(a, b) <- Hash(String, Int) OK
    it('should match Hash(a, b) <- Hash(String, Int)', function() {
        assert.ok(THashAB.match(THashStringInt));
    });
    // Hash(a, a) <- Hash(String, Int) FAIL -- both should be the same types
    it('should not match Hash(a, a) <- Hash(String, Int)', function() {
        assert.ok(! THashAA.match(THashStringInt));
    });
    it('should not match Hash(a, a) <- Hash(a, b)', function() {
        assert.ok(! THashAA.match(THashAB));
    });
    it('should match Hash(a, b) <- Hash(a, a)', function() {
        assert.ok(THashAB.match(THashAA));
    });
    
    // Function([Int], b) ...
    it('should match a type with a list in a parameter', function() {
        assert.ok(TClosureInt.match(TClosureInt));
    });
    it('should not match a type with a different list in a parameter', function() {
        assert.ok(! TClosureInt.match(TClosureString));
    });
    
    // Function([a], b) ...
    it('should match a type with a list in a parameter and variable name', function() {
        assert.ok(TClosureAA.match(TClosureAA));
    });
    it('should not match a type with a different list in a parameter and variable name', function() {
        assert.ok(! TClosureAA.match(TClosureAB));
    });
});

describe('Type List matching', function() {
    it('cannot work yet, TODO', function() {
		assert.ok(false, 'TODO');
	});
});
