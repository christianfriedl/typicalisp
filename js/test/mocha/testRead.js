"use strict";

const deps = {
    assert: require('assert'),
    SimpleLisp: require('SimpleLisp.js'),
    util: require('util'),
};

describe('read', function() {
    it('should read an int atom', function() {
        const text = '23';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', expr);
        deps.assert.deepEqual({ dataType: 'Int', value: 23 }, expr);
    });
    it('should read a float atom', function() {
        const text = '23.1';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', expr);
        deps.assert.deepEqual({ dataType: 'Float', value: 23.1 }, expr);
    });
    it('should read a string atom', function() {
        const text = '"text"';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', expr);
        deps.assert.deepEqual({ dataType: 'String', value: 'text' }, expr);
    });
    it('should read a sexpr', function() {
        const text = '(+ 23 24)';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', expr);
        deps.assert.deepEqual([ 
            { dataType: 'Symbol', value: '+' },
            { dataType: 'Int', value: 23 },
            { dataType: 'Int', value: 24 },
        ], expr);
    });
    it('should read a first-nested sexpr', function() {
        const text = '(+ (+ 23 24) 25)';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', expr);
        deps.assert.deepEqual(
            [ 
                { dataType: 'Symbol', value: '+' },
                [
                    { dataType: 'Symbol', value: '+' },
                    { dataType: 'Int', value: 23 },
                    { dataType: 'Int', value: 24 },
                ],
                { dataType: 'Int', value: 25 },
            ], 
        expr);
    });
    it('should read a second-nested sexpr', function() {
        const text = '(+ 23 (+ 24 25))';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', expr);
        deps.assert.deepEqual(
            [ 
                { dataType: 'Symbol', value: '+' },
                { dataType: 'Int', value: 23 },
                [
                    { dataType: 'Symbol', value: '+' },
                    { dataType: 'Int', value: 24 },
                    { dataType: 'Int', value: 25 },
                ],
            ],
        expr);
    });
    it('should read a complex nonsense sexpr', function() {
        const text = '(inc (+ 23 (+ 24 25) 1 "abc" -1) (sca x y 3) 3)';
        const tokens = deps.SimpleLisp.tokenize(text);
        const expr = deps.SimpleLisp.read(tokens);
        console.log('expr', deps.util.inspect(expr, { depth: null }));
        deps.assert.deepEqual(
            [ { dataType: 'Symbol', value: 'inc' },
              [ { dataType: 'Symbol', value: '+' },
                { dataType: 'Int', value: 23 },
                [ { dataType: 'Symbol', value: '+' },
                  { dataType: 'Int', value: 24 },
                  { dataType: 'Int', value: 25 } ],
                { dataType: 'Int', value: 1 },
                { dataType: 'String', value: 'abc' },
                { dataType: 'Int', value: -1 } ],
              [ { dataType: 'Symbol', value: 'sca' },
                { dataType: 'Symbol', value: 'x' },
                { dataType: 'Symbol', value: 'y' },
                { dataType: 'Int', value: 3 } ],
              { dataType: 'Int', value: 3 } ],
        expr);
    });
});
