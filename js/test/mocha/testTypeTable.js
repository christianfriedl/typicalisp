"use strict";

const deps = {
    TypeTable: require('TypeTable.js'),
    assert: require('assert'),
    inspect: require('util.js').inspect,
};

const inspect = deps.inspect;

describe('findType', function() {
    it('should find type Number', function() {
        const tt = deps.TypeTable.createDefaultTypeTable();
        const rv = tt.findTypeByName('Number');
        deps.assert.strictEqual('Number', rv.name);
    });
    it('should find type Int', function() {
        const tt = deps.TypeTable.createDefaultTypeTable();
        const rv = tt.findTypeByName('Int');
        deps.assert.strictEqual('Int', rv.name);
    });
    it('should not find type foo', function() {
        const t = deps.TypeTable.createDefaultTypeTable();
        const rv = t.findTypeByName('foo');
        deps.assert.ok(!rv);
    });
});
