const deps = {
    Type: require('Type.js'),
    TypicaLisp: require('TypicaLisp.js'),
    Scope: require('Scope.js'),
    assert: require('assert'),
};
const inspect = require('util.js').inspect;

function typeCheckText(tl, text) {
    const tokens = tl.tokenize(text);
    const scope = tl.getGlobalScope();
    const expr = tl.read(tokens);
    rv = tl.typeCheck(expr, scope);
    return rv;
}
describe.only('Static type checker', function() {
    it('should check a literal expression', function() {
        const tl = deps.TypicaLisp.create();
        const actual = typeCheckText(tl, '3');
        deps.assert.deepEqual(actual, tl.getRootType().findTypeByName('Int'));
    });
    it('should check +', function() {
        const tl = deps.TypicaLisp.create();
        const actual = typeCheckText(tl, '(+ 1 2)', deps.Scope.createGlobalScope());
        deps.assert.deepEqual(actual, tl.getRootType().findTypeByName('Number'));
    });
    it.only('should check type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = typeCheckText(tl, '(type Int)',deps.Scope.createGlobalScope());
        deps.assert.deepEqual(actual, tl.getRootType().findTypeByName('Type'));
    });
    it.only('should check :', function() {
        const tl = deps.TypicaLisp.create();
        const actual = typeCheckText(tl, '(: (type Int) i)',deps.Scope.createGlobalScope());
        deps.assert.deepEqual(actual, tl.getRootType().findTypeByName('TypedName'));
    });
    it.only('should work on a let', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = typeCheckText(tl, `(let (: (type Int) i) 1 i)`, deps.Scope.createGlobalScope());
        deps.assert.deepEqual(actual, tl.getRootType().findTypeByName('Int'));
    });
    it('should run a let-over-lambda', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(let (: (type Closure) add9) (lambda (list (: (type Int) p)) (type Int) (+ p 9)) (add9 2))`, deps.Scope.createGlobalScope());
        deps.assert.deepEqual(actual, i(tl, 11));
    });
    it('should run a nested list', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run(`(' 1 (' 2 3 ))`,deps.Scope.createGlobalScope());
        deps.assert.deepEqual(l(tl, [i(tl, 1), l(tl, [ i(tl, 2), i(tl, 3) ])]), rv);
    });
    it('should run a flat sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run('(+ 3 4)', deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(tl, 7), rv);
    });
    it('should run a nested sexpr (right)', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run('(+ 1 (+ 2 3))', deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(tl, 6), rv);
    });
    it('should run a nested sexpr (left)', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run('(+ (+ 1 2) 4)', deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(tl, 7), rv);
    });
    it('should run a more complex sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run('(+ (+ 1 2) (+ 5 6))', deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(tl, 14), rv);
    });
    it('should run a more complex sexpr 2', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run('(* (+ 1 2) (+ 5 6))', deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(tl, 33), rv);
    });
});
describe('Static Type Checker', function() {
    it('should typecheck an Int', function() {
        const text = '1';
        const sexpr = parse(text);
        deps.assert.equals(sexpr.getType(), rootType.findTypeByName('Int'));
    });
    it('should typecheck an Int sexpr', function() {
        const text = '(+ 1 1)';
        const sexpr = parse(text);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        deps.assert.equals(sexpr.getDataType(), rootType.findTypeByName('Int'));
    });
    it('should typecheck a covariant assignment', function() {
        const text = `
            (let (: Int a) 0 a)
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false, 'whole expr should be of type Int');
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should typecheck a covariant parameter passing', function() {
        const text = `
            (let (: Int a) 0 (+1 a))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should NOT typecheck a contravariant assignment', function() {
        const text = `
            (let (: Int a) (/ 1 2) a)
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should NOT typecheck a contravariant parameter passing', function() {
        const text = `
            (let (: Number a) 0 (+1 a))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should typecheck and restrict a covariant assignment', function() {
        const text = `
            (let (: Object a) (/ 1 2) a)
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        // assert: the whole expr is a Number
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('...', function() {
        const text = `
            (function foo (' ) Int
                (if true 1 0)
            )
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should assign, and the type should be correct', function() {
        const text = `
            (set 
                div
                (lambda (' (: Int a) (: Int b)) Float
                    (/ a b)))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should assign, preserving the type', function() {
        const text = `
            (set 
                (: (type Function (' Int Int) Float) div)
                (lambda (' (: Int a) (: Int b)) Float
                    (/ a b)))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should assign, restricting the type', function() {
        const text = `
            (set 
                (: (type Function (' Object Object) Object) div)
                (lambda (' (: Int a) (: Int b)) Float
                    (/ a b)))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should NOT typecheck a lambda whose inner expression would be contravariant to the declared type', function() {
        // should fail
        const text = `
            (lambda (' (: Int a) (: Int b)) Int (/ a b)))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
    it('should NOT typecheck an assignment from f :: Int -> Int -> Float to f :: Int -> Int -> Int', function() { 
        // should fail
        const text = `
            (set 
                (: (type Function (' Int Int) Int) div)
                (lambda (' (: Int a) (: Int b)) Float
                    (/ a b)))
        `;
        const sexpr = parse(text);
        const actualType = deps.Lisp.checkType(sexpr);
        deps.assert.ok(false);
        deps.assert.ok(deps.Lisp.checkType(sexpr));
        //deps.assert.equals(sexpr.getResultType(), rootType.findTypeByName('Int'));
    });
});
