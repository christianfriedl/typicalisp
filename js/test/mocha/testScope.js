"use strict";

const deps = {
    assert: require('assert'),
    Scope: require('Scope.js'),
    Value: require('Value.js'),
    TypeTable: require('TypeTable.js'),
    Type: require('Type.js'),
};

describe.only('Scope', function() {
    it('should find the normal "+" operation', function() {
        const gs = deps.Scope.createGlobalScope();
        const rv = gs.findGenerator('+', [ deps.Value.create(deps.TypeTable.create().findTypeByName('Int'), 1), deps.Value.create(deps.TypeTable.create().findTypeByName('Int'), 2) ], false);
        deps.assert.strictEqual('Generator', rv.getAtomType());
    });
    it('should not find the normal "foo" operation', function() {
        const gs = deps.Scope.createGlobalScope();
        const rv = gs.findGenerator('foo', [ deps.Value.create(deps.TypeTable.create().findTypeByName('Int'), 1), deps.Value.create(deps.TypeTable.create().findTypeByName('Int'), 2) ], false);
        deps.assert.ok(!rv);
    });
});

describe('findNormalFunction', function() {
    it('should find nonspecial operator + with two int args', function() {
        const scope = deps.Scope.globalScope();
        const rv = deps.Scope.findNormalFunction(scope, '+', [ { dataType: 'Int' }, { dataType: 'Int' } ]);
        deps.assert.strictEqual(rv.debugName, '+');
    });
    it('should find nonspecial operator + with three int args', function() {
        const scope = deps.Scope.globalScope();
        const rv = deps.Scope.findNormalFunction(scope, '+', [ { dataType: 'Int' }, { dataType: 'Int' }, { dataType: 'Int' } ]);
        deps.assert.strictEqual(rv.debugName, '+');
    });
    it('should not find nonspecial operator + with a string arg', function() {
        const scope = deps.Scope.globalScope();
        const rv = deps.Scope.findNormalFunction(scope, '+', [ { dataType: 'String' }, { dataType: 'Int' }, { dataType: 'Int' } ]);
        deps.assert.ok(!rv);
    });
    it('should not find nonspecial operator + with Int and a string arg', function() {
        const scope = deps.Scope.globalScope();
        const rv = deps.Scope.findNormalFunction(scope, '+', [ { dataType: 'Int' }, { dataType: 'String' }, { dataType: 'Int' } ]);
        deps.assert.ok(!rv);
    });
    it('should find special operator if', function() {
        const scope = deps.Scope.globalScope();
        const rv = deps.Scope.findSpecialFunction(scope, 'if');
        deps.assert.strictEqual(rv.debugName, 'if');
    });
    it('should find special operator iffy', function() {
        const scope = deps.Scope.globalScope();
        const rv = deps.Scope.findSpecialFunction(scope, 'iffy');
        deps.assert.ok(!rv);
    });
});
