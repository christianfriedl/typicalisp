"use strict";

const deps = {
    TypicaLisp: require('TypicaLisp.js'),
    Scope: require('Scope.js'),
    _: require('lodash'),
    assert: require('assert'),
    util: require('util'),
    Atom: require('Atom.js'),
    TypeTable: require('TypeTable.js'),
};

function inspect(i) { return deps.util.inspect(i); }
function i(i) { return deps.Atom.create(deps.TypeTable.create().findTypeByName('Int'), i); }
function f(f) { return deps.Atom.create(deps.TypeTable.create().findTypeByName('Float'), f); }
function s(s) { return deps.Atom.create(deps.TypeTable.create().findTypeByName('String'), s); }
function b(b) { return deps.Atom.create(deps.TypeTable.create().findTypeByName('Bool'), b); }
function l(l) { return deps.Atom.create(deps.TypeTable.create().findTypeByName('List'), l); }

describe('adhoc test', function() {
    it('should simplest', function() {
        const tl = deps.TypicaLisp.create();
        const rv = tl.run(`
            
            (+ 1 2)
            `);
        deps.assert.deepEqual(i(3), rv);
    });
    it.only('should match parameterized types', function() { 
        const tl = deps.TypicaLisp.create();
        const rv = tl.run(`
            (match-types (:: Function (' (' Int) Int)) (:: Function (' (' Int) Int)))
        `, deps.Scope.createGlobalScope());
        deps.assert.deepEqual(b(true), rv);
    });
});
