"use strict";

const deps = {
    'assert': require('assert'),
    'Atom': require('Atom.js'),
};

describe('Atom', function() {
    [ 'Type', 'Value', 'Generator', 'Name' ].forEach(function(type) {
        it('should create an Atom of type ' + type, function() {
            const atom = deps.Atom.create(type);
            deps.assert.strictEqual(type, atom.getAtomType());
        });
    });
    it('should blow up on illegal atomType', function() {
        deps.assert.throws(function() {
            const atom = deps.Atom.create('abla');
        });
    });
});
