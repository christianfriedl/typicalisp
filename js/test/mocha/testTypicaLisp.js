"use strict";

const deps = {
    TypicaLisp: require('TypicaLisp.js'),
    Scope: require('Scope.js'),
    _: require('lodash'),
    assert: require('assert'),
    Atom: require('Atom.js'),
    TypeTable: require('TypeTable.js'),
    util: require('util'),
    Log: require('Log.js'),
};

const inspect = require('util.js').inspect;

function getRootType(tl) {
    const scope = tl.getGlobalScope();
    const rt = scope.getRootType();
    return rt;
}
function i(tl, val) { return tl.createAtom('Int', val); }
function f(tl, val) { return tl.createAtom('Float', val); }
function s(tl, val) { return tl.createAtom('String', val); }
function b(tl, val) { return tl.createAtom('Bool', val); }
function l(tl, elType, val) { 
    const rootType = tl.getRootType();
    if ( deps._.isString(elType) ) {
        elType = rootType.findTypeByName(elType);
    }
    const listType = rootType.createTypeFromName('List');
    listType.setVariant({ 'el': elType });
    return deps.Atom.create(listType, val); 
}
function t(tl, typeName) { 
    const scope = tl.getGlobalScope();
    const rt = scope.getRootType();
    const type = rt.findTypeByName(typeName);
    return tl.createAtom('Type', type); 
}
function tn(tl, typeName, objectName) { 
    const scope = tl.getGlobalScope();
    const rt = scope.getRootType();
    const type = rt.findTypeByName(typeName);
    return scope.createAtom('TypedName', { dataType: type, name: objectName });
}

describe('up to apply', function() {
    it('should run a numeric literal', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
            3
        `);
        let expected = i(tl, 3);
        deps.assert.deepEqual(actual, expected);
    });
    it('should run +', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
            (+ 3 2)
        `);
        let expected = i(tl, 5);
        deps.assert.deepEqual(actual, expected);
    });
    it('should run apply on a god-given function', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
            (apply + 3 2)
        `);
        let expected = i(tl, 5);
        deps.assert.deepEqual(actual, expected);
    });
    it('should run apply on a user-defined function', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
            (function foo (' (: (type Int) i)) (type Int) (+ 3 i))
            (apply foo 2)
        `);
        let expected = i(tl, 5);
        deps.assert.deepEqual(actual, expected);
    });
    it('should run apply on a let-defined function', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
            (let (: (type Closure (' (' (type Int)) (type Int))) foo) (lambda (' (: (type Int) i)) (type Int) (+ 3 i)) (apply foo 3))
        `);
        let expected = i(tl, 6);
        deps.assert.deepEqual(actual, expected);
    });
    it('should run apply on a closure', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
            (apply (lambda (' (: (type Int) i)) (type Int) (+ 3 i)) 3)
        `);
        let expected = i(tl, 6);
        deps.assert.deepEqual(actual, expected);
    });
});
describe('Tokenizer', function() {
    it('should deal with comments', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run(`
                // line comment
                (+ 1 2) 
            // `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1 2) // line comment`);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1 2) // line comment
        `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1 // line comment
                2)
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1// line comment
                2)
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (// line comment
                + 1 2)
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                ( // line comment
                + 1 2)
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                /* multiline comment
                 */ (+ 1 2) 
            /* */ `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                /* multiline comment
                 */(+ 1 2) 
            /* */ `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1 2) /* multiline comment */
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1 /* multiline comment */
                2) 
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1 /* multiline 
                    comment */
                2) 
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (+ 1/* multiline 
                    comment */
                2) 
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run(`
                (/* multiline 
                    comment */
                + 1 2) 
            `);
        deps.assert.deepEqual(actual, i(tl, 3));
    });
});
describe('Newly basic functions', function() {
    it('should run a literal expression', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run('3');
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run('3.2');
        deps.assert.deepEqual(actual, f(tl, 3.2));
        actual = tl.run('"heastoida"');
        deps.assert.deepEqual(actual, s(tl, 'heastoida'), actual);
    });
    it('should run an empty string', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run('""');
        deps.assert.deepEqual(actual, s(tl, ''));
    });
    it('should run +', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 1 2)');
        deps.assert.deepEqual(i(tl, 3), actual);
    });
    it('should run -', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(- 5 2)');
        deps.assert.deepEqual(i(tl, 3), actual);
    });
    it('should run *', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(* 3 2)');
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should run /', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(/ 1 2)');
        deps.assert.deepEqual(f(tl, 0.5), actual);
    });
    it('should run :', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(: (type Int) i)');
        deps.assert.deepEqual(tn(tl, 'Int', 'i'), actual);
    });
    it('should run list', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(list 1 2 3)');
        deps.assert.deepEqual(l(tl, 'Int', [ i(tl, 1), i(tl, 2), i(tl, 3) ]), actual);
    });
    it('should run simple type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(type Int)');
        deps.assert.deepEqual(t(tl, 'Int'), actual);
    });
    it('should run parameterized type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(type List (list a))');
        const rootType = tl.getRootType();
        const type = rootType.createTypeFromName('List');
        type.setVariant({ 'el': 'a' });
        const expected = tl.createAtom('Type', type);
        deps.assert.deepEqual(actual, expected);
    });
    it('should scream bloody murder on an undefined function', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(function() {
            const actual = tl.run(`(let 1)`);
        });
    });
    it('should create a lambda', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(lambda (list (: (type Int) p)) (type Int) (+ p 9))`);
        deps.assert.equal(actual.dataType.name, 'Closure');
        // possibly TODO: check more details of actual
    });
    it('should create a closure type', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
                    (type 
                        Closure 
                        (list 
                            (list 
                                (type Int)
                            ) 
                            (type Int)
                        )
                    )`);
        const expected = '{"dataType":{"dataType":"Type","name":"Type","orTypes":[],"parameterNames":[],"variant":null,"members":{}},"value":{"dataType":"Type","name":"Closure","orTypes":[],"parameterNames":["inTypes","returnType"],"variant":{"inTypes":[{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}],"returnType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}},"members":{}}}'
        deps.assert.deepEqual(JSON.stringify(actual), expected);
    });
    it('should assign a closure type to a completely defined closure variable', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (let 
                (: 
                    (type 
                        Closure 
                        (list 
                            (list 
                                (type Int)
                            ) 
                            (type Int)
                        )
                    ) 
                    add9
                ) 
                (lambda (list (: (type Int) p)) (type Int) (+ p 9)) add9)`);
        const expected = '{"dataType":{"dataType":"Type","name":"Closure","orTypes":[],"parameterNames":["inTypes","returnType"],"variant":{"inTypes":[{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}],"returnType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}},"members":{}}}';
        deps.assert.deepEqual(JSON.stringify(actual), expected);
    });
    it('should assign a closure type to a raw closure variable', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (let 
                (: 
                    (type 
                        Closure 
                    ) 
                    add9
                ) 
                (lambda (list (: (type Int) p)) (type Int) (+ p 9)) add9)`);
        const expected = '{"dataType":{"dataType":"Type","name":"Closure","orTypes":[],"parameterNames":["inTypes","returnType"],"variant":{"inTypes":[{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}],"returnType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}},"members":{}}}';
        deps.assert.deepEqual(JSON.stringify(actual), expected);
    });
    it('should run a let-over-lambda', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (let 
                (: 
                    (type 
                        Closure 
                        (list 
                            (list 
                                (type Int)
                            ) 
                            (type Int)
                        )
                    ) 
                    add9
                ) 
                (lambda (list (: (type Int) p)) (type Int) (+ p 9)) (add9 2))`);
        deps.assert.deepEqual(actual, i(tl, 11));
    });
    it('should run a nested list', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(' 1 (' 2 3 ))`);
        deps.assert.deepEqual(l(tl, 'Object', [i(tl, 1), l(tl, 'Int', [ i(tl, 2), i(tl, 3) ])]), actual);
    });
    it('should run a flat sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 3 4)');
        deps.assert.deepEqual(i(tl, 7), actual);
    });
    it('should run a nested sexpr (right)', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 1 (+ 2 3))');
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should run a nested sexpr (left)', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ (+ 1 2) 4)');
        deps.assert.deepEqual(i(tl, 7), actual);
    });
    it('should run a more complex sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ (+ 1 2) (+ 5 6))');
        deps.assert.deepEqual(i(tl, 14), actual);
    });
    it('should run a more complex sexpr 2', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(* (+ 1 2) (+ 5 6))');
        deps.assert.deepEqual(i(tl, 33), actual);
    });
});
describe('System constants', function() {
    it('should know system constants', function() {
        const tl = deps.TypicaLisp.create();
        let actual = tl.run('true');
        deps.assert.deepEqual(b(tl, true), actual);
        actual = tl.run('false');
        deps.assert.deepEqual(b(tl, false), actual);
        actual = tl.run('pi');
        deps.assert.deepEqual(f(tl, Math.PI), actual);
    });
});
describe('Basic functions', function() {
    it('should run a literal expression', function() {
        let actual;
        const tl = deps.TypicaLisp.create();
        actual = tl.run('3');
        deps.assert.deepEqual(actual, i(tl, 3));
        actual = tl.run('3.2');
        deps.assert.deepEqual(actual, f(tl, 3.2));
        actual = tl.run('"heastoida"');
        deps.assert.deepEqual(actual, s(tl, 'heastoida'), actual);
    });
    it('should run a flat list', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(' 1 2 3)`);
        deps.assert.deepEqual(actual, l(tl, 'Int', [i(tl, 1), i(tl, 2), i(tl, 3)]));
    });
    it('should run a nested list', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(' 1 (' 2 3 ))`);
        deps.assert.deepEqual(l(tl, 'Object', [i(tl, 1), l(tl, 'Int', [ i(tl, 2), i(tl, 3) ])]), actual);
    });
    it('should run a flat sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 3 4)');
        deps.assert.deepEqual(i(tl, 7), actual);
    });
    it('should run a nested sexpr (right)', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 1 (+ 2 3))');
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should run a nested sexpr (left)', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ (+ 1 2) 4)');
        deps.assert.deepEqual(i(tl, 7), actual);
    });
    it('should run a more complex sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ (+ 1 2) (+ 5 6))');
        deps.assert.deepEqual(i(tl, 14), actual);
    });
    it('should run a more complex sexpr 2', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(* (+ 1 2) (+ 5 6))');
        deps.assert.deepEqual(i(tl, 33), actual);
    });
    it('should run a more complex sexpr 3', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(/ (+ 5 10) (+ 2 1))');
        deps.assert.deepEqual(f(tl, 5), actual);
    });
    it('should run a more complex sexpr 4', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(- (+ 5 10) (+ 2 1))');
        deps.assert.deepEqual(i(tl, 12), actual);
    });
    it('should run a complex list/sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 5 (sum (\' 2 3 4)))');
        deps.assert.deepEqual(i(tl, 14), actual);
    });
    it('should run a complex nested sexpr', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(- 14 (* 2 (+ 3 4)))');
        deps.assert.deepEqual(i(tl, 0), actual);
    });
    it('should run a complex nested sexpr 2', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(- (* 2 (+ 3 4)) 14)');
        deps.assert.deepEqual(i(tl, 0), actual);
    });
    it('should run two lists, returning the last result', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(\' 1 2) (\' 3 4)');
        deps.assert.deepEqual(l(tl, 'Int', [ i(tl, 3), i(tl, 4) ]), actual);
    });
    it('should run two lists in one list', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (' 
                (' 1 2) 
                (' 3 4)
            )
        `);
        const expected = '{"dataType":{"dataType":"Type","name":"List","orTypes":[],"parameterNames":["el"],"variant":{"el":{"dataType":"Type","name":"List","orTypes":[],"parameterNames":["el"],"variant":{"el":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}},"members":{}}},"members":{}},"value":[{"dataType":{"dataType":"Type","name":"List","orTypes":[],"parameterNames":["el"],"variant":{"el":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}},"members":{}},"value":[{"dataType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}},"value":1},{"dataType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}},"value":2}]},{"dataType":{"dataType":"Type","name":"List","orTypes":[],"parameterNames":["el"],"variant":{"el":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}},"members":{}},"value":[{"dataType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}},"value":3},{"dataType":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}},"value":4}]}]}';
        deps.assert.deepEqual(expected, JSON.stringify(actual));
    });
    it('should run a let and return the value', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(let (: (type Int) x) 2 x)');
        deps.assert.deepEqual(i(tl, 2), actual);
    });
    it('should run an untyped let and return the value', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(let x 23 x)');
        deps.assert.deepEqual(i(tl, 23), actual);
    });
    it('should run a let and use the value', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(let (: (type Int) x) 2 (+ x 2))');
        deps.assert.deepEqual(i(tl, 4), actual);
    });
    it('should throw in let on bad typecheck Int<-Float', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => {
            const actual = tl.run('(let (: (type Int) x) 22.2 2)');
        });
    });
    it('should throw in let on bad typecheck Int<-String', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => {
            const actual = tl.run('(let (: (type Int) x) "abc" 2)');
        });
    });
    it('should run a let in a list but nothing else', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(let (: (type Int) x) 1 (\' x 2))');
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1), i(tl, 2)]), actual);
    });
    it('should run two lets on top level', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(let (: (type Int) x) 1 (+ x 2)) (let (: (type Int) x) 2 (+ x 4))');
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should run a set and return the value', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(set (: (type Int) x) 15) x');
        deps.assert.deepEqual(i(tl, 15), actual);
    });
    it('should run a set, add a let, and return the value', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(set (: (type Int) x) 15) (let (: (type Int) y) x y)');
        deps.assert.deepEqual(i(tl, 15), actual);
    });
    it('should run a set and use the value', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(set (: (type Int) x) 1) (+ x 2)');
        deps.assert.deepEqual(i(tl, 3), actual);
    });
    it('should error on set if run twice', function() { 
        deps.assert.throws( () => {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run('(set (: (type Int) x) 15) (+ x 2) (set (: (type Int) x) 2) (+ x 2)');
        });
    });
    it('should throw in set on bad typecheck Int<-Float', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => {
            const actual = tl.run('(set (: (type Int) x) 22.2 2)');
        });
    });
    it('should throw in set on bad typecheck Int<-String', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => {
            const actual = tl.run('(set (: (type Int) x) "abc" 2)');
        });
    });
    it('should run two adds in one list', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(\' (+ 1 2) (+ 2 4))');
        deps.assert.deepEqual(l(tl, 'Int', [ i(tl, 3), i(tl, 6 )]), actual);
    });
    it('should run two lets in one list', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(\' (let (: (type Int) x) 1 (+ x 2)) (let (: (type Int) x) 2 (+ x 4)))');
        deps.assert.deepEqual(l(tl, 'Int', [ i(tl, 3), i(tl, 6 )]), actual);
    });
    it('should run several sexprs, returning last result', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 1 2) (+ 8 4)');
        deps.assert.deepEqual(i(tl, 12), actual);
    });
    it('should run several sexprs, returning last result 2', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(+ 3 9) (+ 17 4) (+ 8 3)');
        deps.assert.deepEqual(i(tl, 11), actual);
    });
    it('should throw on nonexistant operator', function() {
        deps.assert.throws(() => {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run('(does-not-exist)');
        });
    });
    it('should throw on operator w/ unmatched operand types', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => {
            const actual = tl.run(`(/ "a" "b")`);
        });
    });
});
describe('Basic arithmetic', function() {
    it('+', function() {
        const tl = deps.TypicaLisp.create();
        let actual = tl.run('(+ 1 2)');
        deps.assert.deepEqual(i(tl, 3), actual);
        actual = tl.run('(+ 1.2 2)');
        deps.assert.deepEqual(f(tl, 3.2), actual);
    });
    it('-', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(- 2 1)');
        deps.assert.deepEqual(i(tl, 1), actual);
    });
    it('*', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(* 4 2)');
        deps.assert.deepEqual(i(tl, 8), actual);
    });
    it('/', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(/ 8 2)');
        deps.assert.deepEqual(f(tl, 4), actual);
    });
    it('%', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(% 5 3)');
        deps.assert.deepEqual(i(tl, 2), actual);
    });
    it('++', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(++ 1)');
        deps.assert.deepEqual(i(tl, 2), actual);
    });
    it('--', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(-- 1)');
        deps.assert.deepEqual(i(tl, 0), actual);
    });
});
describe('Comparison operators', function() {
    it('should run =', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(= 1 1)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('= neg', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(= 1 2)');
        deps.assert.deepEqual(b(tl, false), actual);
    });
    it('=', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(= 1 1)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('<', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(< 1 2)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('>', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(> 2 1)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('<=', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(<= 1 2)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('>=', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(>= 2 1)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('!=', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(!= 1 2)');
        deps.assert.deepEqual(b(tl, true), actual);
    });
});
describe('String functions', function() {
    it('+', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (+ "foo" "bar")
            `);
        deps.assert.deepEqual(s(tl, 'foobar'), actual);
    });
});
describe('Lambdas', function() {
    it('should create a lambda w/out running it', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(let (: (type Closure (list (list (type Int)) (type Int))) a-lambda) (lambda (list (: (type Int) p)) (type Int) (+ p 9)) 2)`);
        deps.assert.deepEqual(i(tl, 2), actual);
    });
    it('should run a lambda', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(let (: (type Closure (list (list (type Int)) (type Int))) a-lambda) (lambda (list (: (type Int) p)) (type Int) (+ p 9)) (a-lambda 2))`);
        deps.assert.deepEqual(i(tl, 11), actual);
    });
    it('should run lambda w/ capture', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(let (: (type Int) x) 7 (let (: (type Closure (list (list (type Int)) (type Int))) a-lambda) (lambda (list (: (type Int) p)) (type Int) (+ p x)) (a-lambda 2)))`);
        deps.assert.deepEqual(i(tl, 9), actual);
    });
    it('should throw on missing return type', function() { 
        deps.assert.throws(() => {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run('(lambda (\' (: Int p)) (+ p 9) Int)');
        });
    });
    it('should run a global lambda w/set', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(set (: (type Closure (list (list (type Int)) (type Int))) a-lambda) (lambda (list (: (type Int) p)) (type Int) (+ p 9))) (a-lambda 2)`);
        deps.assert.deepEqual(i(tl, 11), actual);
    });
    it('should throw on nonexisting type name', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => {
            const actual = tl.run('(set (: Frunction mylam) (lambda (\' (: Inti p)) Int (+ p 9) Int)) (mylam 2)');
        });
    });
    it('should apply a lambda-notassigned function by parameter', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function receiver (' (: (type Closure (' (' (type Int)) (type Int))) closure) (: (type Int) j)) (type Int) (closure j))
            (receiver (lambda (' (: (type Int) i)) (type Int) (+ 2 i)) 4)
            `);
        // we will give the error message "type Closure has no parameter inTypes", which is very very bad indeed
        // we should see that "type" has not enough argumens instead!
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should throw on a lambda with mismatched returntype', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`(let (: (type Closure) add9) (lambda (list (: (type Int) p)) (type String) (+ p 9)) (add9 2))`);
        deps.assert.deepEqual(actual, i(tl, 11));
    });
});
describe('Functions', function() {
    it('should create and evaluate a global function', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(function add2 (\' (: (type Int) x)) (type Int) (+ x 2)) (add2 4)');
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should create and evaluate a global function, but unused', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(function add2 (\' (: (type Int) x)) (type Int) (+ x 2)) (+ 3 4) (+ 9 8)');
        deps.assert.deepEqual(i(tl, 17), actual);
    });
    it('should create and evaluate a global function, but used once', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(function add2 (\' (: (type Int) x)) (type Int) (+ x 2)) (+ (add2 4) (+ 9 8))');
        deps.assert.deepEqual(i(tl, 23), actual);
    });
    it('should create and evaluate a global function, but used at end', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(function add2 (\' (: (type Int) x)) (type Int) (+ x 2)) (+ 3 4) (add2 8)');
        deps.assert.deepEqual(i(tl, 10), actual);
    });
    it('should create and evaluate a global function, returning last result', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(function add2 (\' (: (type Int) x)) (type Int) (+ x 2)) (add2 4) (add2 8)');
        deps.assert.deepEqual(i(tl, 10), actual);
    });
    it('should create and evaluate a recursive function', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(function recur (\' (: (type Int) x)) (type Int) (if (= x 0) 0 (recur (- x 1)))) (recur 20)');
        deps.assert.deepEqual(i(tl, 0), actual);
    });
    it('should create and evaluate a function with parameterized type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function foo (' (: (type List (' (type Int))) l)) (type List (' (type Int))) l) (foo (' 1))`
        );
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1)]), actual);
    });
    it('should create and evaluate a function with abstractly paramaterized type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function foo (' (: (type List (' a)) l)) (type List (' a)) l) (foo (' 1))`
        );
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1)]), actual);
    });
});
describe('Indirect function application', function() {
    it('should apply a user-defined function via a variable', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function argument (' (: (type Int) i)) (type Int) (+ 2 i))
            (function receiver (' (: (type Closure (' (' (type Int)) (type Int))) closure) (: (type Int) j)) (type Int) (closure j))
            (receiver argument 4)
            `);
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should apply a god-given function via a variable', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function receiver (' (: (type Closure (' (' (type Int)) (type Int))) closure) (: (type Int) j)) (type Int) (closure j))
            (receiver ++ 4)
            `);
        deps.assert.deepEqual(i(tl, 5), actual);
    });
    it('should apply a lambda-assigned function via a variable', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (set 
                (: 
                    (type 
                        Closure 
                        (list 
                            (list 
                                (type Int)
                            ) 
                            (type Int)
                        )
                    ) 
                    argument
                ) 
                (lambda 
                    (list 
                        (: (type Int) i)
                    ) 
                    (type Int) 
                    (+ 2 i)
                )
            )
            (function receiver 
                (list 
                    (: 
                        (type 
                            Closure 
                            (list 
                                (list (type Int))
                                (type Int)
                            ) 
                        ) 
                        closure
                    ) 
                    (: (type Int) j)
                ) 
                (type Int) 
                (closure j)
            )
            (receiver argument 4)
            `);
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should apply a function-assigned function by parameter', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function 
                receiver 
                (list 
                    (: 
                        (type Closure (list (list (type Int)) (type Int))) 
                        closure
                    ) 
                    (: 
                        (type Int) 
                        j
                    )
                ) 
                (type Int) 
                (closure j)
            )
            (receiver 
                (lambda 
                    (list 
                        (: 
                            (type Int) 
                            i
                        )
                    ) 
                    (type Int) 
                    (+ 2 i)
                ) 
                4
            )
            `);
        deps.assert.deepEqual(i(tl, 6), actual);
    });
    it('should run an apply', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (apply + 2 3)
            `);
        deps.assert.deepEqual(i(tl, 5), actual);
    });
    it('should directly apply a lambda via apply', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (apply (lambda (' (: (type Int) i)) (type Int) (++ i)) 22)
        `);
            // (let (: (type Closure (' (' Int) Int)) foo) (lambda (' (: (type Int) i)) (type Int) (++ i)) (foo 22))
        deps.assert.deepEqual(i(tl, 23), actual);
    });
    it.skip('TODO should run an eval', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(eval (\' 10 20 30))');
        deps.assert.deepEqual(10, actual);
    });
});
describe('Conditional functions', function() {
    it('should run a positive if', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(if 1 2 3)');
        deps.assert.deepEqual(i(tl, 2), actual);
    });
    it('should run a negative if', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(if 0 2 3)');
        deps.assert.deepEqual(i(tl, 3), actual);
    });
    it('should run an if with a list', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(if (+ 1 2) (\' 3 4) 3)');
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 3), i(tl, 4)]), actual);
    });
    it('should run a negative if with a list', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(if 0 (\' 3 4) 8)');
        deps.assert.deepEqual(i(tl, 8), actual);
    });
    it('should run a cond (1st  choice)', function() { 
        const tl = deps.TypicaLisp.create();
        let actual;
        actual = tl.run('(cond (= 1 1) "one" (= 1 2) "two" (= 1 3) "three")');
        deps.assert.deepEqual(s(tl, "one"), actual);
    });
    it('should run a cond (2nd choice)', function() { 
        const tl = deps.TypicaLisp.create();
        let actual;
        actual = tl.run('(cond (= 2 1) "one" (= 2 2) "two" (= 1 3) "three")');
        deps.assert.deepEqual(s(tl, "two"), actual);
    });
    it('should run a cond w/catchall', function() { 
        const tl = deps.TypicaLisp.create();
        let actual;
        actual = tl.run('(cond (= 0 1) "one" (= 0 2) "two" (= 0 3) "three" true "none")');
        deps.assert.deepEqual(s(tl, "none"), actual);
    });
    it('should run normally after a cond', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(cond (= 1 1) "one" (= 0 2) "two" (= 0 3) "three" true "none") (+ 1 2)');
        deps.assert.deepEqual(i(tl, 3), actual);
    });
});
describe('List functions', function() {
    it.skip('TODO should run list w/ empty string', function() {
        // da hängt sich der tokenizer auf
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(list "")');
        deps.assert.deepEqual(l(tl, 'String', [ s(tl, '') ]), actual);
    });
    it('should run empty list', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(list )');
        deps.assert.deepEqual(l(tl, null, []), actual);
    });
    it('should run count', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(count (\' 2 3 4))');
        deps.assert.deepEqual(i(tl, 3), actual);
    });
    it('should run sum', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(sum (\' 2 3 4))');
        deps.assert.deepEqual(i(tl, 9), actual);
    });
    it('should run a head', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(head (\' 10 20 30))');
        deps.assert.deepEqual(i(tl, 10), actual);
    });
    it('should run a tail', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(tail (\' 10 20 30))');
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 20), i(tl, 30)]), actual);
    });
    it('should run a get', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('(get 2 (\' 10 20 30 40))');
        deps.assert.deepEqual(i(tl, 30), actual);
    });
    it('should not run a worngful get', function() { 
        const tl = deps.TypicaLisp.create();
        deps.assert.throws(() => { const actual = tl.run('(get 4 (\' 10 20 30 40))'); });
    });
    /// TODO test ALL list functions!
    it('should run a concat', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(concat (' 1 2 3) (' 4 5 6))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1), i(tl, 2), i(tl, 3), i(tl, 4), i(tl, 5), i(tl, 6)]), actual);
    });
    it('should run find', function() {  // TODO unimplemented functions
        // TODO this does not work yet
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (find (lambda (' (: (type Int) i)) (type Bool) (= 2 i)) (' 1 2 3))
            `);
        deps.assert.deepEqual(i(tl, 2), actual);
    });
    it('should run a reverse', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(reverse (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 3), i(tl, 2), i(tl, 1)]), actual);
    });
    it('should run a length', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(length (' 1 2 3 4 5))");
        deps.assert.deepEqual(i(tl, 5), actual);
    });
    it('should run a push', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(push 6 (' 1 2 3 4 5))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1), i(tl, 2), i(tl, 3), i(tl, 4), i(tl, 5), i(tl, 6)]), actual);
    });
    it('should run a get', function() { 
        const tl = deps.TypicaLisp.create();
        let actual = tl.run("(get 0 (' 1 2 3))");
        deps.assert.deepEqual(i(tl, 1), actual);
        actual = tl.run("(get 1 (' 1 2 3))");
        deps.assert.deepEqual(i(tl, 2), actual);
        deps.assert.throws(function() {
            let actual = tl.run("(get 3 (' 1 2 3))");
        });
    });
    it('should run a replace', function() { 
        const tl = deps.TypicaLisp.create();
        let actual;
        actual = tl.run("(replace 199 0 (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 199), i(tl, 2), i(tl, 3)]), actual);
        actual = tl.run("(replace 199 1 (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1), i(tl, 199), i(tl, 3)]), actual);
        deps.assert.throws(function() {
            let actual = tl.run("(replace 3 (' 1 2 3))");
        });
    });
    it('should run an insert', function() { 
        const tl = deps.TypicaLisp.create();
        let actual;
        actual = tl.run("(insert 199 0 (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 199), i(tl, 1), i(tl, 2), i(tl, 3)]), actual);
        actual = tl.run("(insert 199 2 (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 1), i(tl, 2), i(tl, 199),  i(tl, 3)]), actual);
        deps.assert.throws(function() {
            let actual = tl.run("(insert 4 (' 1 2 3))");
        });
    });
    it('should run a map w/lambda', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(map (lambda (' (: (type Int) x)) (type Int) (+ 1 x)) (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 2), i(tl, 3), i(tl, 4)]), actual);
    });
    it('should run a map by function name', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(set (: (type Closure (' (' (type Int)) (type Int))) foo) (lambda (' (: (type Int) x)) (type Int) (+ 1 x))) (map foo (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 2), i(tl, 3), i(tl, 4)]), actual);
    });
    it('should run a map by builtin function name', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run("(map ++ (' 1 2 3))");
        deps.assert.deepEqual(l(tl, 'Int', [i(tl, 2), i(tl, 3), i(tl, 4)]), actual);
    });
});
describe.skip('TODO Partial application', function() {
    it('should return a partially applied function', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function adder (' (: Int i) (: Int j)) Int (+ i j))
            (adder 1)
        `);
        deps.assert.ok(actual);
    });
    it('should apply partially, then fully', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function adder (' (: Int i) (: Int j)) Int (+ i j))
            (set oneAdder (adder 1))
            (oneAdder 2)
        `);
        deps.assert.deepEqual(i(tl, 3), actual);
    });
});
describe.skip('TODO File functions', function() {
    it('should load and eval a file', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (load "js/test/mocha/test.tl") testvariable
        `);
        deps.assert.deepEqual(i(tl, 23), actual);
    });
});
describe('Type system', function() {
    it('should return a simple type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run('Int');
        const expected = '{"dataType":{"dataType":"Type","name":"Type","orTypes":[],"parameterNames":[],"variant":null,"members":{}},"value":{"dataType":"Type","name":"Int","orTypes":[],"parameterNames":[],"variant":null,"members":{}}}';
        deps.assert.deepEqual(expected, JSON.stringify(actual));
    });
    it('= should throw on unmatched types', function() {
        deps.assert.throws(function() {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run('(= 1 1.0)');
        });
    });
    it('should throw on existing type', function() {
        deps.assert.throws(function() {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run(`
                (make-type Int)
            `);
        });
    });
    it.skip('TODO should create a simple type', function() {
        const tl = deps.TypicaLisp.create();
        const scope = deps.Scope.createGlobalScope();
        const actual = tl.run(`
            (make-type Foo (type Object))
        `, scope);
        const rt = scope.getRootType();
        deps.assert.deepEqual(rt.findTypeByName('Foo'), actual.value);
    });
    it.skip('TODO should create a parameterized type', function() {
        const tl = deps.TypicaLisp.create();
        const scope = deps.Scope.createGlobalScope();
        const actual = tl.run(`
            (make-type Foo (type Object (list param)))
        `, scope);
        const rt = scope.getRootType();
        deps.assert.deepEqual(rt.findTypeByName('Foo'), actual.value);
    });
    it('should match matching types', function() {
        const tl = deps.TypicaLisp.create();
        const scope = deps.Scope.createGlobalScope();
        const actual = tl.run(`
            (match (type Number) (type Int))
        `, scope);
        const rt = scope.getRootType();
        deps.assert.deepEqual(b(tl, true), actual);
    });
    it('should not match not matching types', function() {
        const tl = deps.TypicaLisp.create();
        const scope = deps.Scope.createGlobalScope();
        const actual = tl.run(`
            (match (type Number) (type String))
        `, scope);
        const rt = scope.getRootType();
        deps.assert.deepEqual(b(tl, false), actual);
    });
    it('should match parameterized type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(
            `(match (type List (' (type Int))) (type List (' (type Int))))
        `);

        const expected = tl.createAtom('Bool', true);
        deps.assert.deepEqual(actual, expected);
    });
    it('should not match unmatching parameterized type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(
            `(match (type List (' (type Int))) (type List (' (type String))))
        `);
        const rootType = tl.getRootType();

        const expected = tl.createAtom('Bool', false);
        deps.assert.deepEqual(actual, expected);
    });
    it('should match abstractly parameterized type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(
            `(match (type List (' a)) (type List (' a)))
        `);
        const rootType = tl.getRootType();
        const type = rootType.createTypeFromName('List');
        type.setVariant({ 'el': 'a' });

        const expected = tl.createAtom('Bool', true);
        deps.assert.deepEqual(actual, expected);
    });
    /*

* Function type checking w/ abstract types: 
    * (\ (' (: (type List (' a) l))) a () 
* Function type checking w/ abstract dependent types: 
    * (\ (' (: (type List (' a) l))) a (head l))  --> success, will work under any context, a === a
    * ((\ (' (: (type List (' a) l))) a (length l) (' 1))  --> success, Int === Int
    * ((\ (' (: (type List (' a) l))) a (length l) (' "x"))  --> failure, Int !== String

     */
});
describe.skip('TODO Type inference', function() {
    it('should infer an int literal', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            23
        `);
        deps.assert.ok(false, 'TODO');
    });
    it('should infer a float literal', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            23.5
        `);
        deps.assert.ok(false, 'TODO');
    });
    it('should infer a string literal', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            "abc"
        `);
        deps.assert.ok(false, 'TODO');
    });
    it('should infer a list literal', function() { 
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (' "abc" "def")
        `);
        deps.assert.ok(false, 'TODO');
    });
    it('should create and evaluate a function with abstractly parameterized type and related return type', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function foo (' (: (type List (' a)) l)) a 1) (foo (' 1))`
        );
        deps.assert.deepEqual(i(tl, 1), actual);
    });
    it('should reject a function with abstractly paramaterized type and related unmatching return type', function() {
        deps.assert.throws(() => {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run(`
                (function foo (' (: (type List (' a)) l)) a "string") (foo (' 1))`
            );
        });
    });
});
describe('Macros', function() {
    it('should create a macro', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(` // a very useless minimal macro
            (macro foo exprs exprs Nothing)
        `);
        const expected = '{"dataType":{"dataType":"Type","name":"Macro","orTypes":[],"parameterNames":[],"variant":null,"members":{}}}';
        deps.assert.deepEqual(JSON.stringify(actual), expected);
    });
    it('should create a global macro', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(` // a very useless minimal macro
            (set (: Macro foo) (macro exprs exprs Nothing))
        `);
        const expected = undefined;
        deps.assert.deepEqual(actual, expected);
    });
});
describe('IO', function() {
    it('should read a file', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (read-file "./test/file")
        `);
        const rt = getRootType(tl);
        const type = rt.createTypeFromName('IO');
        type.setVariant({ 'type': rt.findTypeByName('String') });
        const expected = deps.Atom.create(type, 'xyz\n');
        deps.assert.deepEqual(actual, expected);
    });
});
describe('Fallout from bugfixes', function() {
    it('two functions with the same parameter name should not clash', function() {
        const tl = deps.TypicaLisp.create();
        const actual = tl.run(`
            (function add1 (' (: (type Int) i)) (type Int) (+ 1 i))
            (function add2 (' (: (type Int) i)) (type Int) (+ 2 i))
            (' (add1 1) (add2 1))
        `);
        const expected = l(tl, 'Int', [ i(tl, 2), i(tl, 3) ]);
        deps.assert.deepEqual(actual, expected);
    });
    it('one function with the same parameter name twice should crash', function() {
        deps.assert.throws(function() {
            const tl = deps.TypicaLisp.create();
            const actual = tl.run(`
                (function add1 (' (: (type Int) i) (: (type Int) i)) (type Int) (+ 1 i))
            `);
        });
    });
});
