"use strict";

const deps = {
    TypicaLisp: require('TypicaLisp.js'),
    Scope: require('Scope.js'),
    _: require('lodash'),
    assert: require('assert'),
    Value: require('Value.js'),
    TypeTable: require('TypeTable.js'),
};

const inspect = require('util.js').inspect;

function createAtom(typeName, value) {
    return deps.Value.create(deps.TypeTable.create().findTypeByName(typeName), value);
}
function i(val) { return createAtom('Int', val); }
function f(val) { return createAtom('Float', val); }
function s(val) { return createAtom('String', val); }
function b(val) { return createAtom('Bool', val); }
function l(val) { return createAtom('List', val); }
function tn(typeName, objectName) { return createAtom('TypedName', { dataType: deps.TypeTable.create().findTypeByName(typeName), name: objectName }); }

describe('Tail recursion', function() {
    it('should run a tailrec sum', function() {
        let rv;
        const tl = deps.TypicaLisp.create();

        rv = tl.run(`

(function recsum (' (: List l)) Number
    (_recsum l 0))
(function _recsum (' (: List l) (: Number acc)) Number
    (if (= 0 (count l))
        acc
        (_recsum (tail l) (+ (head l) acc))))

(recsum (' 2 3 4 100))

            `, deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(109), rv);
    });
    it.only('should run a tailrec factorial', function() {
        let rv;
        const tl = deps.TypicaLisp.create();

        rv = tl.run(`
(function recfact (' (: Int i)) Int
    (_recfact i 1))
(function _recfact (' (: Int i) (: Int acc)) Int
    (if (= 1 i)
        acc
        (_recfact (- i 1) (* i acc))))

(recfact 10)
            `, deps.Scope.createGlobalScope());
        deps.assert.deepEqual(i(3628800), rv);
    });
});

