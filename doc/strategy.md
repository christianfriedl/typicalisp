* eval
    * is it an atom?
        *YES:
            * is it a symbol?
                * YES:
                    * resolve the name
                        * did we find the name?
                            * YES: return the value
                            * NO: Error, we did not find the name
            * NO: is it a literal?
                * YES:
                    * return the value
    * is it a sexpr?
        * YES:
            * resolve the first sub-expr as an operation
                * resolve the name
                    * found it?
                        * YES: is it a function?
                            * YES: run-it(function, exprs)
                            * NO: is it a variable?
                                * is its value a function?
                                    * YES: run-it(function, exprs)
                                    * NO: error
            * resolve all other sub-exprs as exprs of any type (eval all of them rec'ly)
        * NO: Error, there are no other exprs.
        

* run-it(function, exprs):
    * is the function special?
        * YES: run it with the exprs
        * NO:
            * create a list
            * run it with the list

* Resolve any name, regardless of "name Type":
    * Find the name
    * Check the argument types, if any
