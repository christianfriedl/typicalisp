* Macros and Functions
    * Macros are different types than Functions
    * Functions receive a List of Parameters with named types,
        and return a defined type
    * Macros receive a List of Sexprs, have no defined return type,
        but instead have a method to determine their return type statically
* Types
    * Simple Types
        * Just a name
    * OR Types (i.e., type hierarchy)
        * A name
        * A list of ORed types
    * Parameterized Types
        * A name
        * Named Parameters
            * which can be 
                * MetaTypes
                * List of MetaTypes
    * MetaTypes
        * a name and
            * a Type
            * or a Type variable (just a name)
* Type Matching
    * Simple
        * Char 
        * Char <- Char OK
        * Char <- String FAIL
    * OR Types
        * Number = OR(Int, Float)
        * Number <- Number OK
        * Number <- Int OK
        * Int <- Int OK
        * Number <- Float OK
        * Float <- Int FAIL
        * Int <- Number FAIL
    * Parameterized Types
        * Without Variables
            * List(Int) <- List(Int) OK
            * List(Int) <- List(Float) FAIL
            * Hash(Foo, Bar) <- List(Foo) FAIL
        * With Variables
            * List(a) <- List(Int) OK
            * List(a) <- List(b) OK
            * List(Int) <- List(a) FAIL
            * Hash(String, Int) <- Hash(String, a) FAIL
            * Hash(String, a) <- Hash(String, Int) OK
            * Hash(String, a) <- Hash(String, b) OK
            * Hash(a, a) <- Hash(String, String) OK
            * Hash(a, b) <- Hash(String, String) OK
            * Hash(a, b) <- Hash(String, Int) OK
            * Hash(a, a) <- Hash(String, Int) FAIL
    * Matching strategie
        * List OR List(Int) / List(String)??
        * RHS is list of types
            * ALL need to match (because the result can be any of them)
        * "normal" matching
            * this === other OR (name AND vaiant-params) OR subtype))
            * parameter variant matching:
                * each this.parameter that are types
                    * type? match with other.parameter
                * each this.parameter that are strings
                    * same variable names must be same in other
                        * either name OR type in other
                * each this.parameter that are arrays
                    * ... TODO ...???
        * ([Int, Int]) = ([Int, Int])

                
* Type reconstruction / unification: examples

    * foo(a, b) = a + b
        assign                      T4 = Number OK
            foo                     (T4 = Number, T4 = Number) -> T4 = Number 
                a                   T4 = Number
                b                   T4 = Number
                (rv)                T1
            +                       T4 = Number
                a                   T4 = Number
                b                   T4 = Number

    * (let (lambda foo (' a b) (+ a b)))
        let             (T3 T3) -> T3
            lambda foo  (T4 T5) -> T3 = (T3 T3) -> T3
                a       T4 = T3 = Number
                b       T5 = T3 = Number
                ->      T6 = T3 = Number
            +           T7 = T3 = Number
                a       T8 = T3 = Number
                b       T9 = T3 = Number



* Cases I encounter during the day
    * $a = is_null($b) ? $b : 'some default';
    * $dateString = $requestSpecification->getStartDate() ? $requestSpecification->getStartDate()->format('Y-m-d') : null;



* Type checking: 1. check, 2. transfer

    * assuming (length s) returns the string length, let's make a somewhat useless copy of it...

(let
    (: 
        (type 
            Closure 
            (list 
                String
            ) 
            Int
        ) 
        strlen
    ) 
    (lambda 
        (list 
            (: 
                String 
                s
            )
        ) 
        Int 
        (length 
            s
        )
    )
)

    * with parameterized types

(let
    (:
        (type
            Closure
            (list
                (type
                    List
                    a
                )
                a
            )
        )
        first-element
    )
    (lambda
        (list
            (:
                (type
                    List
                    a
                )
                l
            )
        )
        a
        (get
            1
            l
        )
    )
    (first-element
        (list
            "a"
            "b"
            "c"
        )
    )
)


# Why type inference + OO subtyping can be problematic

let f(obj) =
    obj.x + obj.y

Any class that has both a member x and a member y (of types that support the + operator) would qualify as a possible type for obj and the type inference algorithm would have no way of knowing which one is the one you want.

(set (: (type Closure (list obj)) f) (+ (get obj x) (get obj y)))
