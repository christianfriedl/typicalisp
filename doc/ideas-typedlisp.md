# (revamp sept 2018)

## examples - 1

(set addthem (lambda (list (: Int a) (: Int b)) Int (+ a b)))
(set multthem (lambda (list (: Int a) (: Int b)) Int (* a b)))
(set divthem (lambda (list (: Int a) (: Int b)) Float (/ a b))) ## ahem ahem should be sth like "(Maybe Float)" - we can live with the lie for now

(list (addthem 2 3) (multthem 2 3) (divthem 2 3))

## examples - 2

# Clarify before starting

* variables and lambdas - same or different namespaces?
* how to find special forms?
* every data item has a type, starting right from the scanner up!
    * from scanner:
    * { dataType: 'Int', value: 22 }
    * { dataType: 'Float', value: 22.2 }
    * { dataType: 'String', value: 'some string' }
    * { dataType: 'Name', value: 'f' } ### not a Symbol, or is it?!
        * Note, a String is a string literal, a name is a bare word
