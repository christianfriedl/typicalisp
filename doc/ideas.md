* Fundamental Types
    * Thing
        * Operation
            * Variable
            * Function
                * SyntacticFunction
                * SemanticFunction
        * Number
            * Int
            * Float
        * String
        * Char
        * List
        * Sexpr
        * Atom (dataType: String..., Int, hmmm)
        * Symbol (? is that what we call it?)
* If refactoring
    * Use types right from start
    * Correctly return stuff from evals
    * read :: token-stream -> ast/expressions
    * datatypes from read are:
        * Atom / w/dataType
        * Sexpr / w/o/dataType? could only do if we do static typing...
        * List / == sexpr w/'list' function

