= What's in a monad?

1. For each type A of values: a type M[a] represents computations.
* A -> B becomes a -> M[B]

i.e.
    def eval(t: Term): Int
becomes
    def eval(t: Term): M[Int]

2. A way to turn values into computations

    def pure[A](a: A): M[A]

3. A way to combine computations

    def bind[A](m: M[A], k: A => M[B]): M[B]



....

1. a parameterized type
2. a way to turn a value into that type (while computing something)
3. A way to combine computations (i.e, that parameterized type)

