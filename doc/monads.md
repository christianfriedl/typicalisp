https://ericlippert.com/2013/02/28/monads-part-three/

# 1

* Nullable<T>, OnDemand<T>, Lazy<T>

amplifiers

# 2

monad constructorss: new Nullable<T>...

how do you add 1 to a monadic wrapper of an int?

Nullable<Int> addOne(Nullable<Int> i) {
    if ( i.hasValue ) {
        int unwrapped = i.value;
        int result = unwrapped + 1;
        return createNullable(result);
    } else
        return new Nullable<Int>();
}

OnDemand<Int> addOne(OnDemand<Int> i) {
    return () => {
        Int unwrapped = i();
        int result = unwrapped + 1;
        return result;
    }
}

* adding one to a wrapped int somehow produces another wrapped int, and does so in a way that preserves the desired “amplification”.


# 3

# 4

Func<Int, Int> --- operation on integers

Nullable<Int> applyFunction(Nullable<Int> i, Func<Int, Int> func) {
    if ( i.hasValue ) {
        return createNullable<Int>(func(i.value));
    } else {
        return createNullable<Int>();
    }
}


* addOne is now a special case:

Nullable<Int> addOne(Nullable<Int> i) {
    return applyFunction(i, (Int i) => i + 1);
}

* even generaller:

Nullable<T> applyFunction<T>(...) { ... }

* even generaller:

Nullable<R> applyFunction<A, R>(Nullable<A> in, Func<A, R> func) { ... }

Essentially what we’ve got here is a way of turning functions from A to R into functions from M<A> to M<R> such that the action of the function is preserved and the “amplification” provided by the monadic type is also preserved. That is pretty darn useful!

# 5

Nullable<double> SafeLog(int x)
{
  if (x > 0)
    return new Nullable<double>(Math.Log(x));
  else
    return new Nullable<double>();
}

Wenn wir jetzt SafeLog(new Nullable(2)) aufrufen, kommt ein Nullable<Nullable<int>> raus!


Nullable<R> ApplySpecialFunction<A, R>(
  Nullable<A> nullable,
  Func<A, Nullable<R>> function)
{
  if (nullable.HasValue) 
  {
    A unwrapped = nullable.Value;
    Nullable<R> result = function(unwrapped);
    return result;
  }
  else
    return new Nullable<R>();
}


.... avoid redundancy...:

M<R> ApplyFunction<A, R>(
  M<A> wrapped, 
  Func<A, R> function)
{
  return ApplySpecialFunction<A, R>(
    wrapped,
    (A unwrapped) => CreateSimpleM<R>(function(unwrapped)));
}


ApplyFunction :: M a -> ( a -> b) -> M b ====> fish!
ApplySpecialFunction :: M a -> ( a -> M b) -> M b ====> bind!

# 6

Applying the “make a simple wrapper around this value” function to a monad value must produce the same monad value.

The rules of the monad pattern are that a monadic type M<T> provides operations that are logically equivalent to methods:

    static M<T> CreateSimpleM<T>(T t) { ... }
    static M<R> ApplySpecialFunction<A, R>(
      M<A> monad, Func<A, M<R>> function) {...}

subject to the restrictions that:

    ApplySpecialFunction(someMonadValue, CreateSimpleM)

results in a value logically identical to someMonadValue, and that

    ApplySpecialFunction(CreateSimpleM(someValue), someFunction)

results in a value logically identical to

    someFunction(someValue)

# 7

monadic composition helper:

static Func<X, Nullable<Z>> ComposeSpecial<X, Y, Z>(
  Func<X, Nullable<Y>> f,
  Func<Y, Nullable<Z>> g)
{ 
  return x => ApplySpecialFunction(f(x), g);

  The ApplySpecialFunction helper method enables us to apply any function to a monadic type, which is awesome. But in doing so it also enables us to compose any two functions that return that type!

ComposeSpecial :: ( x -> M y ) -> ( y -> M z ) -> ( x -> M z )


ALL RULES:
==========


A monad is a generic type M<T> such that:

    There is some sort of construction mechanism that takes a T and returns an M<T>. We’ve been characterizing this as a method with signature

static M<T> CreateSimpleM<T>(T t)

    Also there is some way of applying a function that takes the underlying type to a monad of that type. We’ve been characterizing this as a method with signature:

static M<R> ApplySpecialFunction<A, R>(
  M<A> monad, Func<A, M<R>> function)

Finally, both these methods must obey the monad laws, which are:

    Applying the construction function to a given instance of the monad produces a logically identical instance of the monad.
    Applying a function to the result of the construction function on a value, and applying that function to the value directly, produces two logically identical instances of the monad.
    Applying to a value a first function followed by applying to the result a second function, and applying to the original value a third function that is the composition of the first and second functions, produces two logically identical instances of the monad.

# 8

Traditional names:

constructor function (createSimpleM) --> unit / return
function application helper (ApplySpecialFunction) ---> bind --- m >>= f  means “bind operation f onto the end of workflow m and give me the resulting new workflow”.

# 9

================================================================================

 M<R> ApplySpecialFunction<A, R>(M<A> wrapped, Func<A, M<R>> function)




====================================================================================

1. irgendwie ein IO (zb von file??) erzeugen (createNullable(Int))

(function-declaration open (' (: String fileName)) (:: IO File))

2. Nullable<Int> applyFunction(Nullable<Int> i, Func<Int, Int> func) {

(function-declaration apply-function (' (: (:: Nullable Int) i) (: (:: Function Int Int))) (:: Nullable Int))

(function-declaration apply-function (' (: (:: IO File) io) (: (:: Function File File))) (:: IO File))
(function-declaration apply-function (' (: (:: IO a) io) (: (:: Function a b))) (:: IO b)) === fish
(function-declaration apply-special-function (' (: (:: IO a) wrapped) (: (:: Function a (:: IO b)) function)) (:: IO b)
zB: (function-declaration apply-special-function (' (: (:: IO File) fileIo) (: (:: Function File (:: IO char)) function)) (:: IO char) === bind
(function-declaration getch (' (: (:: IO File) fileIo))  (:: IO Char))

(bind (open Console) getch)
(bind (getch Console) (\ (' (: Char c in)) (:: IO Char) (putch (+ c 1))))


3. Nullable<R> ApplySpecialFunction<A, R>( Nullable<A> nullable, Func<A, Nullable<R>> function)

createIO :: char -> IO char
open :: filedesc -> IO filedesc // return
getch :: filedesc -> IO char
putch :: IO filedesc -> char -> IO filedesc


====================================================================================

Monad Console

read-line :: Stream -> (IO String)
write-line :: String  -> (IO Stream)

(bind
    (bind
        (bind 
            createConsole 
            read-line
        )
        (\ (' (: String s)) Console (createIO (concat "Hello..." s)))
    )
    write-line
)



(main
    ('
        read-line
        (\ (' (: String s)) Console (createIO (concat "Hello..." s)))
        write-line
    )
)


=====================================================================================
2019-11
=====================================================================================

return :: a -> M a
bind :: (a -> M a) -> (a -> M b) -> M b

get-console :: () -> Console
read-line :: Console -> IO String
write-line :: String -> IO Console

return Console -> IO Console

(bind (return get-console) read-line)

