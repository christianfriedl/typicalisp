* Constructors
* (: (:: Function (' Int Int) Int) f)
* match - v0.2: 
    * Function (' Int Int) Int --> Function YES
    * Function (' Int Int) Int --> Function (' String Int) Int NO
    * Function (' Int Int) Int --> Function (' Number Number) Number ???? --- NO, at least for now!
* v1.0:
    * Function (' a a) a
* We want:
    * paramtererized types
    * dependent types
    * type algebra
        * (name-type int-without-0 (type int) (\ (' (: int argument)) bool (<> argument 0)))
        * (name-type positive-int (type int) (\ (' (: int argument)) bool (> argument 0)))
        * (name-type negative-0-int (type int) (\ (' (: int argument)) bool (not (match (type positive-int) (typeof argument)))))
        * (name-type negative-0-int (type int) (\ (' (: int argument)) bool (and (<> argument 0 (not (match (type positive-int) (typeof argument))))))) 
                * // slgihtly useless example, would be easier tgo just ask for < of course

# Types again - new ideas

in haskell/alike pseudocode:

class Person is
    Customer
    | Provider
    | Employee
;

class Person where
    makePerson(String firstName, String lastName): Person
    getFirstName(Person p): String
    getLastName(Person p): String
;

class Customer where
    makeCustomer(String firstName, String lastName, Int accountNumber): Customer
    getAccountNumber(Customer c): Int
;


* syntaxes we definitely need:
    * (type <existing-type-name> <type-parameter-list>)
        * i.e. ... TODO ...
    * (set <name> (class <is-condition> <where-list>))
        * i.e. (set Person (class (ors Customer Provider Employee) (' (: (type Function (' String String) Person) make-person) (: (type Function (' Person) String) first-name))))
* how to create a type and check it
    * we need to evaluate the sexpr so we can have the relationship
    * hence we need to search/add add the referenced types
    * the sexpr needs a global scope in which only or/and/not are defined
* .... types will be created in the global scope manufacturing function

* We need anonymous types!

(if <...> 1 "abc") -> (type null (or Int String) null)

* On type creation, check

How do parameterized types work?

