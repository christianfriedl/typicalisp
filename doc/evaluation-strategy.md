 # evaluation strategy

 1 

 => tokenizer => { dataType: 'Int', value: 1 }
 => eval 
      no list, no name, just return the input
 => { dataType: 'Int', value: 1 }

 ==========================================================================================================

 (+ 1 2)

 => tokenizer => { dataType: 'Sexpr', value: [ { dataType: 'Name', value: '+' }, { dataType: 'Int', value: 1 }, { dataType: 'Int', value: 2 } ] }
 => eval 
      evalSexpr
          resolve '+' in head position
              find normal oper
              use dataTypes of all parameters
          eval 1
          eval 2
          apply +(1, 2)
              +.func(1, 2)
 => { dataType: 'Int', value: 3 }

 ==========================================================================================================

 (list 1 2)

 => tokenizer => { dataType: 'Sexpr', value: [ { dataType: 'Name', value: 'list' }, { dataType: 'Int', value: 1 }, { dataType: 'Int', value: 2 } ] }
 => eval
      evalSexpr
          resolve 'list' in head position
              find special oper
              use no dataTypes
          apply list(tokens(1, 2))
              list.func(tokens(1, 2))
 => { dataType: 'List', value: [ { dataType: 'Int', value: 1 }, { dataType: 'Int', value: 2 } ] }

 ==========================================================================================================

 // normal form, variable and const
 // (assuming (let a 2))
 (+ a 1)

 => tokenizer => { dataType: 'Sexpr', value: [ { dataType: 'Name', value: '+' }, { dataType: 'Name', value: 'a' ], { dataType: 'Int', value: 1 } ] }
 => eval
 => { dataType: 'Int', value: 3 }


 // special form, variable and const
 // (assuming (let a 2))
 (list a 1)
 => tokenizer => { dataType: 'Sexpr', value: [ { dataType: 'Name', value: 'list' }, { dataType: 'Name', value: 'a' ], { dataType: 'Int', value: 1 } ] }
 => eval
 => { dataType: 'List', value: [ { dataType: 'Int', value: 2 }, { dataType: 'Int', value: 1 } ] }

 => tokenizer => { dataType: 'Sexpr', value: [ { dataType: 'Name', value: '+' }, { dataType: 'Name', value: 'a' ], { dataType: 'Int', value: 1 } ] }

 ==========================================================================================================

 // (taking a lambda and applying it)

 (apply f 1 2)


 //// hang on...
    * either f is in head-position, then we need special code to re-evaluate the lambda to a real function
    * or f is NOT in head-position, then 
        * in a normal function, it will just resolve to its { dataType: Function }, which is okay even if we do not know what to do with it
        * in a special function,the code itself will have to deal with it anyway

 ..... hmmmm....
 * how do we figure out which x to take?
 * we have to pass the { 'Name' ... } item!
 * we can only resolve through 'apply' itself

 * name resolution HAS to take place the moment we actually want to apply something ("in head position"), otherwise it remains just a Name...

 * yaaaay, but I KNOW that this f has a specific type, since it is a parameter!


 =====> application is not the same as name resolution!!!!!

 ==========================================================================================================

 // very complex form, only here for illustration
 (lambda (list (: Int a) (: Int b)) Int (+ a b))
 
 => würg


