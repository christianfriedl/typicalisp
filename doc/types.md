simple types:

{ dataType: 'Int', value: ... }

lambda types:

{ dataType: 'Lambda', paramTypes: [ 'Int', 'Int', ... ], returnType: 'Int', value: ... }
