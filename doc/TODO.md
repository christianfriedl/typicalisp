* Function type checking w/ abstract types: 
    * (\ (' (: (type List (' a) l))) a () 
* Function type checking w/ abstract dependent types: 
    * (\ (' (: (type List (' a) l))) a (head l))  --> success, will work under any context, a === a
    * ((\ (' (: (type List (' a) l))) a (length l) (' 1))  --> success, Int === Int
    * ((\ (' (: (type List (' a) l))) a (length l) (' "x"))  --> failure, Int !== String
* Symbols??
* (eval)
* (macro) / (make-macro)
* (make-type NAME PARENT LIST-OF-TYPED-NAMES) --- (make-type Person Object (' (: String first-name) (: String last-name)))
* (make TYPE ...) --- (make Person "Christian" "Friedl")
* (open-file), (read-file) etc
* list: put!, put
* type casting functions!?
* revisit type-checking for SET: sets global state (baaah) , and other stuff could depend on it!
* curry / uncurry
* curry: (function foo (' a b) (+ a b)) ==> (function foo' (' a) (foo b)) 
* uncurry: (function foo (' a b) (+ a b)) /// (uncurry foo 1) -> (\ (' b) (foo 1 b))
* require
* functions on top need to be pre-fetched etc
* static type checking
* type inference
* basic IO monadic functions
* lets / sets
* 'let': new scope should not be created here, but when evaluating the function
* error messages w/line numbers
* optimize!
* more basic constants
* Random numbers: beware of Math.random()! --- http://davidbau.com/encode/seedrandom.js // https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
