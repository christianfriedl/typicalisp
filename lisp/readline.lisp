(read-line (lambda (' (: String s)) Object (write-line s)))

(function repeat-readline (' (: String lastline)) Object
          (read-line
            (lambda (' (: String line)) Object
                     (if (= line "q")
                       (' (write-line line) (write-line "bye"))
                       (' (write-line line) (repeat-readline line))))))
