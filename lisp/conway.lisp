(function field-width (' ) 30)

(function field-height(' ) 30)

(function create-field (' ) (' ))

(function coords-to-index (' (: Int x) (: Int y))
          (+ (* y (field-width)) x))

(function survives? (' (: List field) (: Int x) (: Int y))
          (let countaround (count-around field x y)
              (if (alive? field x y)
                (or
                  (= 2 countaround)
                  (= 3 countaround)
                )
                (= 3 countaround))))

(function count-around (: List field) (: Int x) (: Int y)
          (+
            (count-in-row field x (- y 1))
            (count-in-row field x y)
            (count-in-row field x (+ y 1))))

(function alive? (' (: List field) (: Int x) (: Int y))
          (cond
            (< x 0) false
            (>= field-width x) false
            (< y 0) false
            (>= field-height y) false
            true (= "x" (get-cell field x y))))

(function count-in-row (' (: List field) (: Int middle-x) (: Int y))
          (+
            (alive? field (- middle-x 1) y)
            (alive? field  middle-x y)
            (alive? field (+ middle-x 1) y)))

(function get-cell (' (: List field) (: Int x) (: Int y))
          (pick field (coords-to-index x y)))

(function main (' )
          (let field (create-field)
            (set-initial-pieces field
              (
              (\ (' (: Function next)) create-next-field

