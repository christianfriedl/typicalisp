(set true (= 1 1))
(function gcd (' (: Int m) (: Int n))
          (cond
            (> m n) (gcd n m)
            (= 0 (% n m)) m
            true (gcd (% n m) m)))

(gcd 136 14)

