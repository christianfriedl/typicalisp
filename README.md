# GENERAL IDEA

TypicaLisp is a lisp-y, purely functional, single-assign language with strict, dynamic
type-checking and multiple dispatch at language level.

In other words, the goal is a (much) lesser Haskell, but with lisp-y syntax,
which boils down to a direct correlation of the code and the syntax tree.

(By "lisp-y", I mean "prefix notation with parentheses and special forms".)


# GOALS

* Implement a simple language from scratch
* Learn about type systems
* Learn about functional programming
* Grok the monad

# VERSION 0.1 GOALS

* Basic parsing and running
* Special forms
* get/set/lambda/function
* Fundamental lisp-y functions (+, -, \*, /, pick ...)
* A fundamental type system (Int, Bool, String, Function etc)
* Type inheritance (Number -> Int/Float etc)
* Minimal type inference: allow to *(let i (+ 2 1))* rather than *(let (: Int i) (+ 2 1))*
* Minimal tail recursion optimisation: strictly for form *(function f ... (if (...) BASECASE (f ...)))* only; no "set/get/lambda etc."
* Minimal monad-ized I/O (getch, putch, rand...) even if it's without parameterized types for the time being
* Partial application

# VERSION 0.2 GOALS

* All 0.1 Goals, duh.
* Basic parameterized types
    * map ::: List a -> List a *(function map (' (: (:: Function a a) f) (: (:: List a) l)) (...))*
* Full monad-ized I/O (getch, putch, rand...)

# VERSION 1.0 GOALS

* All 0.2 Goals, duh.
* Full tail recursion optimisation
* Have at least bearable, if not great, performance
* Code cleanup!!!

# PROBABLE GOALS BEYOND 1.0

* Full type inference (that might depend on static typing; probably better be done *inside* tlisp?)

# TAKE HEED

TypicaLisp is in pre-alpha stage.

It is experimental code, and it is not production-ready by a long shot, and probably never will be.
(as of 2019/11, I'm not entirely certain, anymore...)

# DIARY

* 2019-11-18: Static type checking now works almost completely, including macros.
    * Plan for monadic IO is in place.
    * Name: Probably LeMonad.
    * There is a possibility this might actually prove useful.
    * Proof of concept ideas: Game of Life, and a roguelite.
* 2019-09-07: Implemented static type checking. Added macros.
* 2018-10-31: Code Cleanup
* 2018-10-30: Implemented map. Time to move on to the first semi-monad, getch()...
* 2018-10-29: Partial application works. The code is one ugly bietsch, but it does work.
* 2018-10-27: Implemented simplest form of tail recursion optimization; updated pre-1.0 milestone goals
* 2018-10-25: Figured out how to actually inject names into scope; function applications now work directly and indirectly
* 2018-10-23: Start using actual parameterized types, at least for function definitions
* 2018-10-19: Fundamental revamping is still WIP, most basic functions work, we need to get *get, set, lambda* etc going
    * I am looking for a better name, calling this a "lisp" is terribly misleading.
* 2018-09-06: Re-think the type system; types should go all the way down
* 2018-06-14: Start introducing types; reimplement almost all code
* 2018-03-07: Start working on project; start with typeless lisp

# LICENSE

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />TypicaLisp is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
