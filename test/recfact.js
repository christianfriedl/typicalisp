/*
(function recfact (' (: Int i)) Int
    (_recfact i 1))
(function _recfact (' (: Int i) (: Int acc)) Int
    (if (= 1 i)
        acc
        (_recfact (- i 1) (* i acc))))
*/

function recfact(i) {
    return _recfact(i, 1);
}
function _recfact(i, acc) {
    if ( i === 1 ) {
        return acc;
    } else {
        return _recfact(i - 1, i * acc);
    }
}

function iterfact(i) {
    return _iterfact(i, 1);
}
function _iterfact(i, acc) {
    while ( !(i === 1) ) {
        acc = i * acc;
        i = i - 1;
    }
    return acc;
}

function printrecfact(i) {
    console.log('recfact', i, 'is', recfact(i));
}
function printiterfact(i) {
    console.log('iterfact', i, 'is', iterfact(i));
}
function printbothfact(i) {
    printrecfact(i);
    printiterfact(i);
}

printbothfact(1);
printbothfact(2);
printbothfact(3);
printbothfact(4);
printbothfact(5);
printbothfact(10);
printbothfact(20);

