"use strict";
/*
(function recfib (' (: Int i) Int
    (_recfib 1 1 i)))
(function _recfib (' (: Int a) (: Int b) (: Int which)) Int
    (if (= 1 which)
        a
        (_recfib (+ a b) a (- which 1))))
*/

function recfib(i) {
    return _recfib(1, 1, i);
}

function _recfib(a, b, which) {
    // 1, 1, 3
    if ( which  === 1 ) {
        return a;
    } else {
        return _recfib(a + b, a, which - 1);
    }
}

function iterfib(i) {
    return _iterfib(1, 1, i);
}
function _iterfib(a, b, which) {
    while ( !(which === 1) ) {
        let newB = a; /// HEAR HEAR!!!
        let newA = a + b;
        let newWhich = which - 1;

        a = newA;
        b = newB;
        which = newWhich;
    }
    return a;
}

function printrecfib(i) {
    console.log('recfib', i, '->', recfib(i));
}
function printiterfib(i) {
    console.log('iterfib', i, '->', iterfib(i));
}
function printbothfib(i) {
    printrecfib(i);
    printiterfib(i);
}


printbothfib(1);
printbothfib(2);
printbothfib(3);
printbothfib(4);
printbothfib(5);
printbothfib(6);
printbothfib(8);
printbothfib(9);
printbothfib(10);
printbothfib(40);
printbothfib(50);
printbothfib(60);

