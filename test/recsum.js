"use strict";


/*
(function recsum (' (: List l)) Number
    (_recsum l 0))
(function _recsum (' (: List l) (: Number acc)) Number
    (if (= 0 (count l))
        acc
        (_recsum (tail l) (+ (head l) acc))))
*/

function recsum(l) {
    return _recsum(l, 0);
}

function _recsum(l, acc) {
    if ( l.length === 0 ) {
        return acc;
    } else {
        return _recsum(tail(l), head(l) + acc);
    }
}

function head(l) { return l[0]; }
function tail(l) { return l.slice(1); }

function printrecsum(l) {
    console.log('recsum of', l, 'is', recsum(l));
}

/**************************************/

function itersum(l) {
    return _itersum(l, 0);
}
function _itersum(l, acc) {
    while (!(l.length === 0)) {
        acc = head(l) + acc;
        l = tail(l);
    }
    return acc;
}

function printitersum(l) {
    console.log('itersum of', l, 'is', itersum(l));
}

printrecsum([1, 2, 3, 4]);
printitersum([1, 2, 3, 4]);
printrecsum([1, 2, 3, 4, 5]);
printitersum([1, 2, 3, 4, 5]);
printrecsum([8, 2, 0, 4, 3]);
printitersum([8, 2, 0, 4, 3]);
printrecsum([10]);
printitersum([10]);
printrecsum([11, 100, -12]);
printitersum([11, 100, -12]);


